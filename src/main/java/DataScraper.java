import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rocky Camacho on 11/4/2015.
 */
public class DataScraper {

    public static final String TAB = "\t";
    public static final String NEW_LINE = "\n";
    public static final String SPACE = " ";
    public static final String BASE_URL = "https://www.bible.com";
    public static final String USER_AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
    public static final String REFERRER = "http://www.google.com";
    private static final int MAX_RETRIES = 5;
    public static final String VERSION = "King James Version";

    public void start() {
        try {
            List<Book> books = parseBooks(VERSION);
            System.out.println("storing books: " + books);
            storeBooks(books);
            List<Book> bookList = books.subList(0, books.size());
            for (Book book : bookList) {
                List<BookChapter> bookChapters = parseBookChapterList(book.link, VERSION);
                System.out.println("storing book chapters: " + bookChapters);
                storeBookChapters(book, bookChapters);
                for (BookChapter bookChapter : bookChapters) {
                    System.out.println("parsing book chapter: " + bookChapter.link);
                    parseBookChapterContent(book, bookChapter);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void storeBookChapters(Book book, List<BookChapter> bookChapters) throws IOException {
        File file = new File("chapters_" + book.code + ".tsv");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (BookChapter bookChapter : bookChapters) {
            bw.write(bookChapter.name);
            bw.write(TAB);
            bw.write(bookChapter.link);
            bw.write(NEW_LINE);
            bw.flush();
        }
        bw.close();
    }

    private void storeBooks(List<Book> books) throws IOException {
        File file = new File("books.tsv");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (Book book : books) {
            bw.write(book.code.toUpperCase(Locale.US));
            bw.write(TAB);
            bw.write(book.name);
            bw.write(TAB);
            bw.write(book.link);
            bw.write(NEW_LINE);
            bw.flush();
        }
        bw.close();
    }

    private List<BookChapter> parseBookChapterList(String link, String version) throws IOException {
        List<BookChapter> bookChapters = new ArrayList<>();
        int retries = 0;
        Document document = null;
        while (document == null && retries < MAX_RETRIES) {
            try {
                document = Jsoup.connect(BASE_URL + link)
                        .userAgent(USER_AGENT)
                        .referrer(REFERRER)
                        .get();
            } catch (Exception e) {
                e.printStackTrace();
                retries++;
                try {
                    Thread.sleep(retries * 5000);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        Elements elements = document.select("ol#chapter_selector > li > a");
        for (Element element : elements) {
            bookChapters.add(new BookChapter(element.text().trim(), element.attr("href"), version));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bookChapters;
    }

    private List<Book> parseBooks(String version) throws IOException {
        List<Book> books = new ArrayList<>();
        Document document = Jsoup.connect(BASE_URL + "/bible/1/gen.1.kjv")
                .userAgent(USER_AGENT)
                .referrer(REFERRER)
                .get();
        Elements elements = document.select("div#menu_book > div.scroll > ul > li > a");
        for (Element element : elements) {
            books.add(new Book(element.attr("data-book"), element.text().trim(), element.attr("href"), version));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return books;
    }

    private void parseBookChapterContent(Book book, BookChapter bookChapter) throws IOException {
        int retries = 0;
        Document document = null;
        while (document == null && retries < MAX_RETRIES) {
            try {
                document = Jsoup.connect(BASE_URL + bookChapter.link)
                        .userAgent(USER_AGENT)
                        .referrer(REFERRER)
                        .get();
            } catch (Exception e) {
                e.printStackTrace();
                retries++;
                try {
                    Thread.sleep(retries * 5000);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        File file = new File(bookChapter.name + ".tsv");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
        Elements elements = document.select("div.p > span[data-usfm]:gt(0)");
        for (Element element : elements) {
            bw.write(element.attr("data-usfm"));
            bw.write(TAB);
            Elements texts = element.select("span.content");
            for (int i = 0; i < texts.size(); i++) {
                Element textElement = texts.get(i);
                if (i > 0) {
                    bw.write(SPACE);
                }
                bw.write(textElement.text());
            }
            bw.write(NEW_LINE);
            bw.flush();
        }
        bw.close();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
