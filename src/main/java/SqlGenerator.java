import org.apache.commons.lang3.StringEscapeUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

/**
 * Created by Rocky Camacho on 11/8/2015.
 */
public class SqlGenerator {
    public void start() {
        String version = "King James Version";
        List<Book> books = parseBooks(version);
        createBooksTable(books, version);
        List<Verse> verses = parseVerses(books, version);
        createVersesTable(verses);
    }

    private void createVersesTable(List<Verse> verses) {
        Book book1 = verses.get(0).getBook();
        Path path = Paths.get("output\\" + book1.version.toLowerCase() + "\\verses.sql");
        path.toFile().getParentFile().mkdirs();
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.CREATE)) {
//            String str = new StringBuilder()
//                    .append("CREATE TABLE IF NOT EXISTS `verses` ( ")
//                    .append("\n")
//                    .append("_id INT PRIMARY KEY NOT NULL, ")
//                    .append("\n")
//                    .append("text" + " TEXT, ")
//                    .append("\n")
//                    .append("book" + " TEXT, ")
//                    .append("\n")
//                    .append("chapter" + " INT, ")
//                    .append("\n")
//                    .append("number" + " INT, ")
//                    .append("\n")
//                    .append("version" + " TEXT ")
//                    .append("\n")
//                    .append(" );")
//                    .append("\n")
//                    .append("\n")
//                    .toString();
//            bufferedWriter.write(str);
            for (int i = 0; i < verses.size(); i++) {
                Verse verse = verses.get(i);
                bufferedWriter.write("INSERT INTO `VERSE` (`_id`, `TEXT`, `BOOK`, `CHAPTER`, `BOOK_NUMBER`, `VERSION`) VALUES (" + (i + 1) + ", \"" + StringEscapeUtils.escapeJava(verse.text) + "\", '" + verse.book.code + "', " + verse.chapter + ", " + verse.number + ", '" + verse.book.version + "' );");
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Verse> parseVerses(List<Book> books, String version) {
        ArrayList<Verse> verses = new ArrayList<>();
        File dir = new File("data//" +  version.toLowerCase() + "//");
        for(Book book : books) {
            File[] files = dir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().startsWith(book.name);
                }
            });
            Arrays.sort(files, new NaturalOrderComparator());
            for(File file : files) {
                try {
                    System.out.println("file.toPath(): " + file.toPath());
                    List<String> lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
                    for (String line : lines) {
                        String[] args = line.split("\t");
                        String[] split = args[0].split("\\.");
                        int chapter = Integer.valueOf(split[1]);
                        int number = Integer.valueOf(split[2]);
                        String text = args[1];
                        verses.add(new Verse(book, chapter, number, text));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return verses;
    }

    private void createBooksTable(List<Book> books, String version) {
        Path path = Paths.get("output\\" + version.toLowerCase() + "\\books.sql");
        path.toFile().getParentFile().mkdirs();
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.CREATE)) {
//            String str = new StringBuilder()
//                    .append("CREATE TABLE IF NOT EXISTS `books` ( ")
//                    .append("\n")
//                    .append("_id INT PRIMARY KEY NOT NULL, ")
//                    .append("\n")
//                    .append("code" + " TEXT, ")
//                    .append("\n")
//                    .append("name" + " TEXT, ")
//                    .append("\n")
//                    .append("version" + " TEXT ")
//                    .append("\n")
//                    .append(" );")
//                    .append("\n")
//                    .append("\n")
//                    .toString();
//            bufferedWriter.write(str);
            for (int i = 0; i < books.size(); i++) {
                Book book = books.get(i);
                bufferedWriter.write("INSERT INTO `BOOK` (`_id`, `CODE`, `NAME`, `VERSION`) VALUES (" + (i + 1) + ", '" + book.code + "', '" + book.name + "', '" + book.version + "' );");
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Book> parseBooks(String version) {
        List<Book> books = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get("data\\" + version.toLowerCase() + "\\books.tsv"));
            for (String line : lines) {
                String[] args = line.split("\t");
                String code = args[0];
                String name = args[1];
                String link = args[2];
                books.add(new Book(code, name, link, version));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return books;
    }
}
