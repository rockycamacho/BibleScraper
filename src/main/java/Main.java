import java.io.IOException;

/**
 * Created by Rocky Camacho on 11/8/2015.
 */
public class Main {

    public static void main(String[] args) throws IOException {
//        DataScraper scraper = new DataScraper();
//        scraper.start();
        SqlGenerator sqlGenerator = new SqlGenerator();
        sqlGenerator.start();
    }
}
