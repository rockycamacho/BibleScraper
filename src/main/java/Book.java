/**
 * Created by Rocky Camacho on 11/9/2015.
 */
public class Book {

    String code;
    String name;
    String link;
    String version;

    public Book(String code, String name, String link, String version) {
        this.code = code;
        this.name = name;
        this.link = link;
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
